package se.doless.hellu;

import java.util.ArrayList;

import se.doless.hellu.client.HelluClient;
import se.doless.hellu.client.Message;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

/**
 * Messages Activity handles the messages between two users.
 * 
 * @author Christopher Lindblom
 */
public class MessagesActivity extends Activity {
	
	public static final String USER_ID = "se.doless.hellu.client.HelluClient.MessagesActivity.UserId";
	
	MessagesAdapter messagesAdapter;
	HelluClient client;
	ListView messagesListView;
	EditText newMessageEditText;
	String userId;
	ArrayList<Message> messages = new ArrayList<Message>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_messages);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		client = HelluClient.getInstance();
		
		userId = getIntent().getExtras().getString(USER_ID);
		
		messagesListView = (ListView)findViewById(R.id.messagesListView);
		newMessageEditText = (EditText)findViewById(R.id.messageEditText);
	}

	@Override
	protected void onResume() {
		fetchMessages();
		super.onResume();
	}

	/**
	 * Handles the send message button event.
	 * 
	 * @param view the button that fired the event
	 */
	public void sendMessageButtonClick(View view) {
		new SendMessageTask().execute(newMessageEditText.getText().toString());
	}
	
	/**
	 * Loads messages from the server.
	 */
	private void fetchMessages() {
		new UpdateMessagesTask().execute();
	}

	/**
	 * SendMessageTask handles background thread communication for the send message event.
	 */
	private class SendMessageTask extends AsyncTask<String, Void, Message> {

		@Override
		protected Message doInBackground(String... message) {
			return client.sendMessage(userId, message[0]);
		}

		@Override
		protected void onPostExecute(Message result) {
			newMessageEditText.setText("");
			fetchMessages();
			super.onPostExecute(result);
		}
		
	}
	
	/**
	 * UpdateMessagesTask handles background thread communication for the get message event.
	 */
	private class UpdateMessagesTask extends AsyncTask<Void, Void, ArrayList<Message>> {

		@Override
		protected ArrayList<Message> doInBackground(Void... nothing) {
			return client.getMessages(userId);
		}

		@Override
		protected void onPostExecute(ArrayList<Message> result) {
			messagesAdapter = new MessagesAdapter(MessagesActivity.this, result, client.getUserId());
			messagesListView.setAdapter(messagesAdapter);
			super.onPostExecute(result);
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;

		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
}

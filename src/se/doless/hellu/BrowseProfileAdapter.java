package se.doless.hellu;

import java.util.ArrayList;

import se.doless.hellu.client.Profile;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Adapter class for listing profiles (users) in list views.
 * 
 * @author Christopher Lindblom
 */
public class BrowseProfileAdapter extends BaseAdapter {

	private ArrayList<Profile> profiles;
	private static LayoutInflater inflater = null;
	
	/**
	 * Constructor for the BrowseProfileAdapter.
	 * 
	 * @param activity the calling activity
	 * @param profiles profiles container
	 */
	public BrowseProfileAdapter(Activity activity, ArrayList<Profile> profiles) {
		this.profiles = profiles;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		return profiles.size();
	}

	@Override
	public Object getItem(int pos) {
		return profiles.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View view, ViewGroup parent) {
		View vi = view;
		if(view == null)
			vi = inflater.inflate(R.layout.browse_list_item, null);
		
		TextView name = (TextView)vi.findViewById(R.id.browseListName);
		
		Profile profile = profiles.get(pos);
		
		name.setText(profile.getName());
		
		return vi;
	}
	
}

package se.doless.hellu;

import java.util.ArrayList;

import se.doless.hellu.client.HelluClient;
import se.doless.hellu.client.Profile;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;


/**
 * MainActivity is the main activity of the applications (currently only shows a user list).
 * 
 * @author Christopher Lindblom
 */
public class MainActivity extends FragmentActivity {

	private BrowseProfileAdapter profileAdapter;
	private ListView browseList;
	private HelluClient client;
	private ArrayList<Profile> profiles;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		client = HelluClient.getInstance();
		
		browseList = (ListView)findViewById(R.id.browseListView);
		browseList.setOnItemClickListener(browseItemClickListener);
	}

	
	@Override
	protected void onResume() {
		super.onResume();
		
		if(checkLogin())
			loadBrowseListView();
	}

	/**
	 * Checks whether the user is logged in or not.
	 * If not, the login activity gets awaken.
	 *  
	 * @return true if logged in.
	 */
	private boolean checkLogin() {
		if (!client.isLoggedIn()) {
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * Logs the user out of the system, before calling checkLogin again to display the login activity.
	 */
	private void logout() {
		client.logout();
		checkLogin();
	}
	
	/**
	 * On item click listener for browse list.
	 */
	private OnItemClickListener browseItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {
			Intent intent = new Intent(getApplicationContext(), MessagesActivity.class);
			Bundle extras = new Bundle();
			extras.putString(MessagesActivity.USER_ID, profiles.get(arg2).getId().toString());
			intent.putExtras(extras);
			startActivity(intent);
		}
	};

	/**
	 * Loads the users from the server into the list.
	 */
	private void loadBrowseListView() {
		new LoadProfilesTask().execute();
	}

	/**
	 * LoadProfilesTask handles background thread communication for the load profiles event.
	 * 
	 * @author Christopher Lindblom
	 */
	private class LoadProfilesTask extends AsyncTask<Void, Void, ArrayList<Profile>> {

		@Override
		protected ArrayList<Profile> doInBackground(Void... nothing) {
			return client.getProfiles();
		}

		@Override
		protected void onPostExecute(ArrayList<Profile> result) {
			profiles = result;
			profileAdapter = new BrowseProfileAdapter(MainActivity.this, profiles);
			browseList.setAdapter(profileAdapter);
			super.onPostExecute(result);
		}
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
		case R.id.logout:
			logout();
			break;

		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
}

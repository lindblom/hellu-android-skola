package se.doless.hellu;

/**
 * Environments for run mode.
 * 
 * @author Christopher Lindblom
 */
public enum Environments {
	DEVELOPMENT,
	PRODUCTION
}

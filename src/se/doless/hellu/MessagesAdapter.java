package se.doless.hellu;

import java.util.ArrayList;

import se.doless.hellu.client.Message;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Adapter class for listing messages in list views.
 * 
 * @author Christopher Lindblom
 */
public class MessagesAdapter extends BaseAdapter {
	
	private ArrayList<Message> messages;
	private LayoutInflater inflater = null;
	private String userId;
	private static final int SENT_TYPE = 0;
	private static final int RECEIVED_TYPE = 1;
	
	/**
	 * Default constructor for MessagesAdapter.
	 * 
	 * @param activity calling activity
	 * @param messages data container
	 * @param userId user id of current user
	 */
	public MessagesAdapter(Activity activity, ArrayList<Message> messages, String userId) {
		super();
		this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.messages = messages;
		this.userId = userId;
	}

	@Override
	public int getCount() {
		return messages.size();
	}

	@Override
	public Object getItem(int index) {
		return messages.get(index);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public int getItemViewType(int position) {
		return messages.get(position).getProfile().getId().equals(userId) ? SENT_TYPE : RECEIVED_TYPE;
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public View getView(int pos, View view, ViewGroup parent) {
		
		View v = view;
		Message message = messages.get(pos);

		if(view == null) {
			switch(getItemViewType(pos)) {
			case SENT_TYPE:
				v = inflater.inflate(R.layout.message_sent_list_item, null);
				break;
			case RECEIVED_TYPE:
				v = inflater.inflate(R.layout.message_received_list_item, null);
				break;
			}
		}
		
		TextView nameTextView = (TextView)v.findViewById(R.id.nameTextView);
		TextView messageTextView = (TextView)v.findViewById(R.id.messageTextView);
		
		nameTextView.setText(message.getProfile().getName());
		messageTextView.setText(message.getMessage());
		
		return v;
	}

}

package se.doless.hellu;

import se.doless.hellu.client.HelluClient;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Login Activity handles logging in and signing up for the service.
 * 
 * @author Christopher Lindblom
 */
public class LoginActivity extends Activity {

	private HelluClient client;
	private EditText usernameEditText;
	private EditText passwordEditText;
	private EditText signUpUsernameEditText;
	private EditText signUpPasswordEditText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		client = HelluClient.getInstance();
		
		usernameEditText = (EditText)findViewById(R.id.usernameEditText);
		passwordEditText = (EditText)findViewById(R.id.passwordEditText);
		signUpUsernameEditText = (EditText)findViewById(R.id.signUpUsernameEditText);
		signUpPasswordEditText = (EditText)findViewById(R.id.signUpPasswordEditText);
		
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}
	
	/**
	 * Eventhandler for the login button.
	 * 
	 * @param view the button that fired the event
	 */
	public void login(View view) {
		new LoginTask().execute(usernameEditText.getText().toString(), passwordEditText.getText().toString());
	}
	
	/**
	 * Eventhandler for the sign up button.
	 * 
	 * @param view the button that fired the event
	 */
	public void signUp(View view) {
		new SignUpTask().execute(signUpUsernameEditText.getText().toString(), signUpPasswordEditText.getText().toString());
	}
	
	/**
	 * LoginTask handles background thread communication for the login event.
	 * 
	 * @author Christopher Lindblom
	 */
	private class LoginTask extends AsyncTask<String, Void, Boolean> {

		@Override
		protected Boolean doInBackground(String... params) {
			return client.login(params[0], params[1]);
		}

		@Override
		protected void onPostExecute(Boolean success) {
			if (success) {
				finish();
			} else {
				Toast.makeText(LoginActivity.this, "Inloggningen misslyckades!", Toast.LENGTH_SHORT).show();
			}
		}
		
	}
	
	/**
	 * SignUpTask handles background thread communication for the sign up event.
	 * 
	 * @author Christopher Lindblom
	 */
	private class SignUpTask extends AsyncTask<String, Void, Boolean> {

		@Override
		protected Boolean doInBackground(String... params) {
			return client.signUp(params[0], params[1]);
		}

		@Override
		protected void onPostExecute(Boolean success) {
			if (success) {
				finish();
			} else {
				Toast.makeText(LoginActivity.this, "Registreringen misslyckades!", Toast.LENGTH_SHORT).show();
			}
		}
		
	}
}

package se.doless.hellu.client;

/**
 * Data class for a profile(user).
 * 
 * @author Christopher Lindblom
 */
public class Profile {
	
	private final String name;
	private final String id;
	
	/**
	 * Default constructor for Profile.
	 * 
	 * @param id user id
	 * @param name username
	 */
	public Profile(String id, String name) {
		super();
		this.name = name;
		this.id = id;
	}

	/**
	 * Getter for name(username).
	 * 
	 * @return name(username)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Getter for the id(user id)
	 * @return id(user id)
	 */
	public String getId() {
		return id;
	}
}

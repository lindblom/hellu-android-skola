package se.doless.hellu.client;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RestWebClientResponse {
	private final int responseCode;
	private final String responseBody;
	
	public RestWebClientResponse(int responseCode, String responseBody) {
		this.responseBody = responseBody;
		this.responseCode = responseCode;
	}
	
	public int getResponseCode() {
		return responseCode;
	}
	
	public String getResponseBody() {
		return responseBody;
	}
	
	public JSONObject getResponseJSON() {
		try {
			return new JSONObject(responseBody);
		} catch (JSONException e) {
			return null;
		}
	}
	
	public JSONArray getResponseJSONArray() {
		try {
			return new JSONArray(responseBody);
		} catch (JSONException e) {
			return null;
		}
	}
}

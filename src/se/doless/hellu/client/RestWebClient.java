package se.doless.hellu.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Wrapper class to make it easier to communicate with the hellu REST-api.
 * 
 * @author Christopher Lindblom
 */
public class RestWebClient {
	private final Map<String, String> DEFAULT_HEADERS = new HashMap<String, String>();
	private final String BASE_URL;
	
	/**
	 * Default constructor for creating a RestWebClient.
	 * 
	 * @param baseUrl base url
	 */
	public RestWebClient(String baseUrl) {
		BASE_URL = baseUrl;
	}
	
	/**
	 * Make a post request to path.
	 * 
	 * @param path path
	 * @param params parameters
	 * @return response object
	 */
	public RestWebClientResponse post(String path, Map<String, String> params) {
		HttpURLConnection connection = null;
		String result = "";
		int statusCode = 0;
		
		try {
			URL url = new URL(BASE_URL + path);
			connection = openHttpURLConnection(url);
			
			connection.setDoOutput(true);
			DataOutputStream out = new DataOutputStream(connection.getOutputStream());
			out.writeBytes(paramsFormater(params));
			
			result = streamToString(connection.getInputStream());
			
			statusCode = connection.getResponseCode();

		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			if(connection != null)
				connection.disconnect();
		}
		
		return new RestWebClientResponse(statusCode, result);
	}
	
	/**
	 * Make a get request to path.
	 * 
	 * @param path path
	 * @return response object
	 */
	public RestWebClientResponse get(String path) {
		HttpURLConnection connection = null;
		String result = "";
		int statusCode = 0;
		
		try {
			URL url = new URL(BASE_URL + path);
			connection = openHttpURLConnection(url);
			
			result = streamToString(connection.getInputStream());
			
			statusCode = connection.getResponseCode();

		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			if(connection != null)
				connection.disconnect();
		}
		
		return new RestWebClientResponse(statusCode, result);
	}
	
	/**
	 * Helper method to suck out all the info from a stream and put it in a string.
	 * 
	 * @param stream incoming stream
	 * @return string of stream content
	 * @throws IOException io exception might occur
	 */
	private static String streamToString(InputStream stream) throws IOException {
		StringBuilder result = new StringBuilder();
		
		InputStreamReader reader = new InputStreamReader(stream);
		BufferedReader br = new BufferedReader(reader);
		String line;
		
		while((line = br.readLine()) != null) {
			result.append(line);
		}
		
		return result.toString();
	}
	
	/**
	 * Helper method to convert a map of parameters to a post body string.
	 * 
	 * @param params map of parameters
	 * @return string formated parameters
	 */
	private static String paramsFormater(Map<String, String> params) {
		StringBuilder sb = new StringBuilder();
		try {
			for (String key : params.keySet()) {
				sb.append(key + "=" + URLEncoder.encode(params.get(key), "UTF-8") + "&");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		sb.deleteCharAt(sb.length() - 1);

		return sb.toString();
	}
	
	/**
	 * Helper method for opening a new http url connection with default headers.
	 * 
	 * @param url url to open http url connection to
	 * @return the http url connection
	 */
	private HttpURLConnection openHttpURLConnection(URL url) {
		try {
			return addDefaultHeaders((HttpURLConnection)url.openConnection());
		} catch (IOException e) {
			return null;
		}
	}
	
	/**
	 * Helper method for adding default headers.
	 * 
	 * @param connection http url connection to add headers for
	 * @return the same http url connection (for chaingin)
	 */
	private HttpURLConnection addDefaultHeaders(HttpURLConnection connection) {
		return addHeaders(connection, DEFAULT_HEADERS);
	}
	
	/**
	 * Add headers to an http url connection.
	 * 
	 * @param connection http url connection to add headers to
	 * @param headers headers to add
	 * @return the same http url connection (for chaingin)
	 */
	private static HttpURLConnection addHeaders(HttpURLConnection connection, Map<String, String> headers) {
		for (String key : headers.keySet()) {
			connection.addRequestProperty(key, headers.get(key));
		}
		
		return connection;
	}

	/**
	 * Add a default header to the collection of default headers.
	 * 
	 * @param key key
	 * @param value value
	 */
	public void addDefaultHeader(String key, String value) {
		DEFAULT_HEADERS.put(key, value);
	}
}

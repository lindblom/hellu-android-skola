package se.doless.hellu.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import se.doless.hellu.Environments;
import se.doless.hellu.Globals;

/**
 * A client to communicate with the Hellu REST-api.
 * 
 * @author Christopher Lindblom
 */
public class HelluClient {

	private String sessionToken;
	private String userId;
	private final RestWebClient REST_CLIENT = new RestWebClient(BASE_URL);
	private static final String BASE_URL;
	private static final String BASE_URL_DEV = "http://192.168.1.23:3000";
	private static final String BASE_URL_PRODUCTION = "http://skolapi.hellu.se";
	private static final String HELLU_TOKEN_NAME = "hellulogintoken";

	/**
	 * Static constructor for selecting the base url depending on environment.
	 */
	static {
		BASE_URL = Globals.env == Environments.DEVELOPMENT ? BASE_URL_DEV
				: BASE_URL_PRODUCTION;
	}
	
	private static HelluClient instance = new HelluClient();

	/**
	 * Get our singleton instance.
	 * 
	 * @return singeton instace
	 */
	public static HelluClient getInstance() {
		return instance;
	}

	/**
	 * Login to hellu.
	 * 
	 * @param username username
	 * @param password password
	 * @return success
	 */
	public boolean login(String username, String password) {

		Map<String, String> params = new HashMap<String, String>();
		params.put("username", username);
		params.put("password", password);

		try {

			RestWebClientResponse response = REST_CLIENT.post("/login", params);

			if (response.getResponseCode() == 200) {
				userId = response.getResponseJSON().getString("id");
				setSessionKey(response.getResponseJSON()
						.getString("sessionKey"));
				return true;
			}

		} catch (JSONException e) {
		}

		return false;
	}

	/**
	 * Setter for session key.
	 * 
	 * @param sessionKey session key
	 */
	private void setSessionKey(String sessionKey) {
		REST_CLIENT.addDefaultHeader(HELLU_TOKEN_NAME, sessionKey);
		this.sessionToken = sessionKey;
	}

	/**
	 * Logout from Hellu.
	 */
	public void logout() {
		sessionToken = null;
	}

	/**
	 * Checks if the user is logged in.
	 * 
	 * @return true if user was logged in
	 */
	public boolean isLoggedIn() {
		return sessionToken != null;
	}
	
	/**
	 * Send a message.
	 * 
	 * @param userId recipient
	 * @param message message
	 * @return the created message
	 */
	public Message sendMessage(String userId, String message) {
		Message newMessage = null;
		Map<String, String> params = new HashMap<String, String>();
		
		params.put("message", message);
		
		RestWebClientResponse response = REST_CLIENT.post("/messages/" + userId, params);

		try {
			if (response.getResponseCode() == 200) {
				JSONObject jsonMessage;
				jsonMessage = response.getResponseJSON();
				
				JSONObject senderProfileJson = jsonMessage.getJSONObject("sender");
				Profile sender = new Profile(senderProfileJson.getString("id"),
						senderProfileJson.getString("username"));
				
				newMessage = new Message(sender, jsonMessage.getString("message"));
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return newMessage;
	}
	
	/**
	 * Get messages from server.
	 * 
	 * @param userId the other part of the conversation
	 * @return list of messages
	 */
	public ArrayList<Message> getMessages(String userId) {
		ArrayList<Message> messages = new ArrayList<Message>();

		RestWebClientResponse response = REST_CLIENT.get("/messages/" + userId);

		try {
			if (response.getResponseCode() == 200) {
				JSONArray jsonArray = response.getResponseJSONArray();
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonMessage;
					jsonMessage = jsonArray.getJSONObject(i);
					
					JSONObject senderProfileJson = jsonMessage.getJSONObject("sender");
					Profile sender = new Profile(senderProfileJson.getString("id"),
							senderProfileJson.getString("username"));
					
					Message message = new Message(sender, jsonMessage.getString("message"));
					messages.add(message);
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return messages;
	}

	/**
	 * Get profiles(users) from server.
	 * 
	 * @return list of profiles
	 */
	public ArrayList<Profile> getProfiles() {
		ArrayList<Profile> profiles = new ArrayList<Profile>();

		RestWebClientResponse response = REST_CLIENT.get("/users");

		try {
			if (response.getResponseCode() == 200) {
				JSONArray jsonArray = response.getResponseJSONArray();
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonProfile;
					jsonProfile = jsonArray.getJSONObject(i);
					Profile profile = new Profile(jsonProfile.getString("id"),
							jsonProfile.getString("username"));
					profiles.add(profile);
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return profiles;
	}

	/**
	 * Getter for the user id.
	 * 
	 * @return user id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sign up for the hellu service.
	 * 
	 * @param username username
	 * @param password password
	 * @return success
	 */
	public Boolean signUp(String username, String password) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("username", username);
		params.put("password", password);

		try {

			RestWebClientResponse response = REST_CLIENT.post("/signup", params);

			if (response.getResponseCode() == 201) {
				userId = response.getResponseJSON().getString("id");
				setSessionKey(response.getResponseJSON()
						.getString("sessionKey"));
				return true;
			}

		} catch (JSONException e) {
		}

		return false;
	}
}

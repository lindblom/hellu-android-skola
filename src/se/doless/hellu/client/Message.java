package se.doless.hellu.client;

/**
 * Data class for a Message.
 * 
 * @author Christopher Lindblom
 */
public class Message {
	private final Profile profile;
	private final String message;
	
	/**
	 * Default constructor for Message.
	 * 
	 * @param profile sender of message
	 * @param message message
	 */
	public Message(Profile profile, String message) {
		super();
		this.profile = profile;
		this.message = message;
	}
	
	/**
	 * Get the user who sent it.
	 * 
	 * @return user profile
	 */
	public Profile getProfile() {
		return profile;
	}
	
	/**
	 * Get the message.
	 * 
	 * @return message
	 */
	public String getMessage() {
		return message;
	}
	
	
}

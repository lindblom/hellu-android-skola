package se.doless.hellu;

/**
 * Global settings.
 * 
 * @author Christopher Lindblom
 */
public class Globals {
	public final static Environments env = Environments.PRODUCTION; 
}